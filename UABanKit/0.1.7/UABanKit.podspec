#
# Be sure to run `pod lib lint UABanKit.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "UABanKit"
  s.version          = "0.1.7"
  s.summary          = "The _`UABABanKit`_ classe is the entry point for working with **userADgents BAN Framework**."
  s.description      = <<-DESC
    The _`UABABanKit`_ classe is the entry point for working with **userADgents BAN Framework**. BAN stand for **B**eacon **A**d **N**etwork.

    You must have a _Client Token_ valid in order to use the framework.

    Please feel free to contact us for any questions, sugestions, remarks, problems or just to say hello, at [uaban@uad.com](mailto:uaban@uad.com). You can also visit [userADgents website](http://www.useradgents.com) for more information about what we do.
    DESC
  s.homepage         = "https://useradgents.atlassian.net/wiki/display/BAN/BAN"
    # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
    s.license          = 'MIT'
    s.author           = { "Joffrey Bocquet" => "j.bocquet@useradgents.com" }
    s.source           = { :git => "ssh://git@extranet.useradgents.com/ban-kit-ios.git", :tag => "v#{s.version.to_s}" }


#    s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'UABanKit' => ['Pod/Assets/*.png']
  }

    s.public_header_files = 'Pod/Classes/Public/*.h'
    s.frameworks = 'CoreFoundation', 'UIKit', 'CoreLocation'
    s.dependency 'AFNetworking'

end
